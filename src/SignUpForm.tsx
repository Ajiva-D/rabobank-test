import React, { useState } from 'react'
import { Formik, Field, Form, FormikHelpers, ErrorMessage } from 'formik';
import * as Yup from "yup";
import YupPassword from 'yup-password';
YupPassword(Yup)

interface Values {
	firstName: string;
	lastName: string;
	email: string;
	password: string;
}

const SignupSchema = Yup.object().shape({
	firstName: Yup.string()
		.required('Required'),
	lastName: Yup.string()
		.required('Required'),
	email: Yup.string()
		.email('Invalid email')
		.required('Required'),
	password: Yup.string()
		.min(8, 'Too Short!')
		.minLowercase(1, 'Must have at least 1 lowercase letter')
		.minUppercase(1, 'Must have at least 1 uppercase letter')
		.test("not-match-names", "Should not contain your first name", (value, form) => {
			let status: boolean = true
			if (value) {
				let firstname: string = form.parent.firstName.toLowerCase();
				let currentPassword = value.toLowerCase();
				status = (!currentPassword.includes(firstname));
			}
			return status
		}).test("not-match-names", "Should not contain your last name", (value, form) => {
			let status = true;
			if (value) {
				let lastname: string = form.parent.lastName.toLowerCase();
				let currentPassword = value.toLowerCase();
				status = (!currentPassword.includes(lastname));
			}
			return status
		})
		.required('Required'),
});

const SignUpForm = () => {
	return (
		<Formik
			initialValues={{
				firstName: '',
				lastName: '',
				email: '',
				password: ''
			}}
			validationSchema={SignupSchema}
			onSubmit={async (
				values: Values,
				{ setSubmitting, resetForm }: FormikHelpers<Values>,
			) => {
				const url: string = 'https://demo-api.now.sh/users';
				try {
					await fetch(url, { method: 'POST', body: JSON.stringify(values) })
					alert('Form submitted successfully!')
					resetForm()
				} catch (error) {
					console.log(error)
				} finally {
					setSubmitting(false)
				}
			}}
		>
			{({ isSubmitting }) => (
				<Form>
					<h1>Rabobank Sign Up</h1>

					<label htmlFor="firstName">First Name</label>
					<Field id="firstName" name="firstName" placeholder="First Name" />
					<div className="error">
						<ErrorMessage name="firstName" />
					</div>

					<label htmlFor="lastName">Last Name</label>
					<Field id="lastName" name="lastName" placeholder="Doe" />
					<div className="error">
						<ErrorMessage name="lastName" />
					</div>

					<label htmlFor="email">Email</label>
					<Field
						id="email"
						name="email"
						placeholder="john@acme.com"
						type="email"
					/>
					<div className="error">
						<ErrorMessage name="email" />
					</div>


					<label htmlFor="password">Password</label>
					<Field
						id="password"
						name="password"
						placeholder="Enter Password"
						type="password"
					/>
					<div className="error">
						<ErrorMessage name="password" />
					</div>

					<button type="submit" disabled={isSubmitting}>{isSubmitting ? "Submitting..." : "Submit"}</button>
				</Form>
			)}

		</Formik>
	)
}

export default SignUpForm